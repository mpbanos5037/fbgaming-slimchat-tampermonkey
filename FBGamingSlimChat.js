// ==UserScript==
// @name         FacebookGaming SlimChat
// @namespace    http://tampermonkey.net/
// @version      0.26
// @source       https://gitlab.com/RaptorPort/fbgaming-slimchat-tampermonkey
// @updateURL    https://gitlab.com/RaptorPort/fbgaming-slimchat-tampermonkey/-/raw/main/FBGamingSlimChat.js
// @downloadURL  https://gitlab.com/RaptorPort/fbgaming-slimchat-tampermonkey/-/raw/main/FBGamingSlimChat.js
// @description  CSS changes for the Facebook gaming chat, to fit more messages on one screen.
// @author       RaptorPort
// @match        https://*.facebook.com/*
// @icon         https://www.google.com/s2/favicons?domain=facebook.com
// ==/UserScript==

function cssChanges() {
    //comment box
    addGlobalStyle('.sj5x9vvc { padding-bottom: 2px !important; }');
    addGlobalStyle('.tw6a2znq { padding-left: 4px !important; }');
    addGlobalStyle('.d1544ag0 { padding-right: 4px !important; }');
    addGlobalStyle('.cxgpxx05 { padding-top: 2px !important; }');
     // changes to message layout / spacing between messages
    addGlobalStyle('.hv4rvrfc { padding-right: 2px !important; }');
    addGlobalStyle('.dati1w0a { padding-left: 6px !important; }');
    addGlobalStyle('.ecm0bbzt { padding-top: 3px !important; }');
    addGlobalStyle('.e5nlhep0 { padding-bottom: 3px !important; }');
    addGlobalStyle('.bvz0fpym { max-width: calc(100%) !important; }');
    //sharper corners
    addGlobalStyle('.qmr60zad { border-bottom-left-radius: 8px !important; }');
    addGlobalStyle('.qlfml3jp { border-top-right-radius: 8px !important; }');
    addGlobalStyle('.inkptoze { border-bottom-right-radius: 8px !important; }');
    addGlobalStyle('.e72ty7fz { border-top-left-radius: 8px !important; }');
    // trim down the box around replies
    addGlobalStyle('.pybr56ya { padding-top: 4px !important; }');
    addGlobalStyle('.bq3qbged { padding-bottom: 30px !important; }');
    // hides the "Hide this message" dialogue
    addGlobalStyle('.pgctjfs5 { display: none !important; }');
    //hide the share button
    addGlobalStyle('.oajrlxb2.gs1a9yip.g5ia77u1.est2fu8x.i2z0utgf.nalm63nx.rbyq7rg5.goun2846.ccm00jje.s44p3ltw.mk2mc5f4.frvqaej8.ed0hlay0.afxsp9o4.jcgfde61.rq0escxv.nhd2j8a9.pq6dq46d.mg4g778l.btwxx1t3.pfnyh3mw.p7hjln8o.kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.tgvbjcpo.hpfvmrgz.jb3vyjys.rz4wbd8a.qt6c0cv9.a8nywdso.l9j0dhe7.i1ao9s8h.esuyzwwr.f1sip0of.du4w35lb.lzcic4wl.abiwlrkh.p8dawk7l.s45kfl79.emlxlaya.bkmhp75w.spb7xbtv.kn9t4qvh.ni8dbmo4.stjgntxs.bxs1uw09 { display: none !important; }');

    //move reactions to the top right of the message
    addGlobalStyle('._6cuq { top: -2px !important; }');
    addGlobalStyle('._6cuq { right: 0px !important; }');
    addGlobalStyle('.btwxx1t3.nc684nl6.bp9cbjyn { margin-right: 33px !important; }');

    // move badges infront of usernames
    addGlobalStyle('.nc684nl6 { display: inline-flex !important; }');
    addGlobalStyle('.btwxx1t3.nc684nl6.bp9cbjyn { direction: rtl !important; }');
    addGlobalStyle('.kkf49tns  { margin-left: -3px !important; }');

    // custom small black scrollbar
    var ss = document.styleSheets[0];
    ss.insertRule('::-webkit-scrollbar { width: 8px; height: 3px;}', 0);
    ss.insertRule('::-webkit-scrollbar-thumb { height: 50px; background-color: #666; border-radius: 3px;}', 0);

    // make usernames bold
    addGlobalStyle('.lrazzd5p  { font-weight: bold !important; }');

    // uncomment to hide user avatar pictures
    //addGlobalStyle('.nqmvxvec.s45kfl79.emlxlaya.bkmhp75w.spb7xbtv.a8c37x1j.fv0vnmcu.rs0gx3tq.l9j0dhe7  { display: none !important; }');

    // hides "Like" and "Reply" buttons and reveals them again when hobering on it with the mouse
    addGlobalStyle('.rj1gh0hx ._6coi { display: none !important; }');
    addGlobalStyle('.rj1gh0hx:hover ._6coi { display: flow-root !important; }');
    addGlobalStyle('._6coi.oygrvhab.ozuftl9m.l66bhrea.linoseic { direction: ltr !important; }');
    addGlobalStyle('.ozuftl9m { margin-left: 8px !important; }');
    addGlobalStyle('.linoseic { padding-top: 0px !important; }');
    addGlobalStyle('.linoseic { min-height: 14px !important; }');

    // hides the "Replying to username" above replies - the original message of the thread will still be displayed
    addGlobalStyle('.rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.jifvfom9.bp9cbjyn.owycx6da.btwxx1t3.d1544ag0.tw6a2znq.jb3vyjys.nkwizq5d.roh60bw9.tvmbv18p.hop8lmos { display: none !important; }');

    // TODO hide follow button that apears next to some users
}

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

function initChatBoxFinder() {
    var sideBar = document.getElementsByClassName("hybvsw6c j83agx80 pfnyh3mw dp1hu0rb l9j0dhe7 o36gj0jk")[0];
    // will trigger when a clicking in the chat area -> check for a new chatbox
    document.addEventListener('click', function(e) {
        setTimeout(function(){
            var targetElement = event.target
            console.log(targetElement)
            var chatElements = document.getElementsByClassName("rq0escxv j83agx80 cbu4d94t eg9m0zos fh5enmmv k4urcfbm");
            for (let i = 0; i < chatElements.length; i++) {
                if (chatElements[i] != undefined && chatElements[i].getAttribute('chatObserver') != 'true') {
                    initChatObserver(chatElements[i]);
                }
            }
        }, 2000);
    });
}

function initChatObserver(chatElement) {
    console.log("Ataching observer to chatbox");
    const options = {
        attributes : true,
        childList : true,
        subTree: true,
        characterData: true,
        attributeOldValue: false,
        characterDataOldValue: false
    };
    var observer = new MutationObserver(processDomChanges);

    observer.observe(chatElement, options);
    chatElement.setAttribute('chatObserver', 'true');

    // process all current messages once
    var messageList = chatElement.childNodes;
    for (let i = messageList.length-1; i >= 0; i--) {
        editMessage(messageList[i]);
    }
}

const processDomChanges = function (mutationRecords, observer) {
    mutationRecords.forEach(function (mutation) {
        mutation.addedNodes.forEach(function (node) {
            editMessage(node.firstChild);
        });
    });
}

function editMessage(msgElement) {
    var likeReplyElement = msgElement.getElementsByClassName("_6coi oygrvhab ozuftl9m l66bhrea linoseic")[0];
    var usernameElement = msgElement.getElementsByClassName("btwxx1t3 nc684nl6 bp9cbjyn")[0];
    if (usernameElement != undefined && likeReplyElement != undefined) {
        usernameElement.insertAdjacentElement("afterbegin", likeReplyElement);
        return true;
    }
    return false;
}

(function() {
    'use strict';
    console.log("FacebookGaming SlimChat is running and checking for URLs ...")
    // facebook uses AVAX, which changes the contents of the page via a script instead of reloading it
    // to get around this the script is active on all of facebook and will poll every 5 seconds for url changes
    var fireOnHashChangesToo = true;
    var pageURLCheckTimer = setInterval (
    function () {
        if ( this.lastPathStr !== location.pathname || this.lastQueryStr !== location.search || (fireOnHashChangesToo && this.lastHashStr !== location.hash)) {
            this.lastPathStr = location.pathname;
            this.lastQueryStr = location.search;
            this.lastHashStr = location.hash;
            // check for livestream URL
            var found = location.pathname.match(".*(\/videos\/[^\/\r\n]*|live\/producer\/dashboard\/[^\/\r\n]*\/COMMENTS)")
            if (found) {
                console.log("Applying FB-Gaming SlimChat changes to " + location.hostname + location.pathname)
                cssChanges();
                initChatObserver(document.getElementsByClassName("rq0escxv j83agx80 cbu4d94t eg9m0zos fh5enmmv k4urcfbm")[0]);
                initChatBoxFinder();
            } else {
                //console.log("Leaving FB-Gaming livestream chat -> stopping SlimChat" + location.hostname + location.pathname)
            }
        }
    }
    , 5000
);
})();


